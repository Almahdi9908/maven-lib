package com.helper;

import java.util.*;

public class Util {

    public static String calculateName(String... names) {

        String result = "";
        for (String val : names)
            result += val + " ";

        return result;
    }

    public static <E extends Number, T extends Collection<E>> Double calculateSum(T collNumbers) {

        Iterator<E> iterator = collNumbers.iterator();
        Double result = 0.0;
        while (iterator.hasNext()) {
            result += iterator.next().doubleValue();
        }

        return result;
    }

}
