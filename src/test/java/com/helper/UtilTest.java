package com.helper;

import org.testng.annotations.Test;

import java.util.Arrays;

import static org.testng.Assert.assertEquals;

public class UtilTest {

    @Test(description = "when array of names is passed then concatenate then with space.")
    public void testCalculateName() {
        String name = Util.calculateName("Mahmood", "Mahdi", "Mohamed");
        assertEquals(name,"Mahmood Mahdi Mohamed ");
    }

    @Test(description = "when collection based passed with any number it will sum all values.")
    public void testCalculateSum() {

        Double sum = Util.calculateSum(Arrays.asList(2, 25, 3));
        assertEquals(sum,30);

    }
}